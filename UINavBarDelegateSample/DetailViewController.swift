//
//  DetailViewController.swift
//  UINavBarDelegateSample
//
//  Created by Jiri Volejnik on 21/01/2019.
//  Copyright © 2019 Fermion. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, UINavigationBarDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        (navigationController as? NavigationController)?.navigationBarDelegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        (navigationController as? NavigationController)?.navigationBarDelegate = nil
        super.viewWillDisappear(animated)
    }

    func navigationBar(_ navigationBar: UINavigationBar, shouldPop item: UINavigationItem) -> Bool {
        let alert = UIAlertController(title: "Do you really want to leave the page?", message: "All changes will be lost", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Stay here", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Leave", style: .destructive, handler: { action in
            self.navigationController?.popViewController(animated: true)
        }))
        
        self.present(alert, animated: true)
        
        return false
    }
}
